package org.onetimelink.net;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.internal.util.$Nullable;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.ssl.SslContext;

public class OnetimeLinkServerInitializer extends ChannelInitializer<SocketChannel> {

    @Inject
    @$Nullable
    private SslContext sslCtx;
    
    @Inject
    private Provider<OnetimeLinkFullHttpHandler> fullHttpHandlerProvider;

    public OnetimeLinkServerInitializer() {
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline p = ch.pipeline();
        if (sslCtx != null) {
            p.addLast(sslCtx.newHandler(ch.alloc()));
        }
        p.addLast("decoder", new HttpRequestDecoder());
        p.addLast("encoder", new HttpResponseEncoder());
        p.addLast("aggregator", new HttpObjectAggregator(1024 * 1024));
        p.addLast("handler", fullHttpHandlerProvider.get());
    }

}

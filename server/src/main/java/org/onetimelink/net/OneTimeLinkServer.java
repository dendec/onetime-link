package org.onetimelink.net;

import com.google.inject.Inject;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LoggingHandler;
import java.security.cert.CertificateException;
import javax.net.ssl.SSLException;
import org.onetimelink.config.ApplicationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OneTimeLinkServer {

    private final static Logger LOGGER = LoggerFactory.getLogger(OneTimeLinkServer.class);

    @Inject
    private ApplicationConfig config;
    
    @Inject
    private OnetimeLinkServerInitializer serverInitializer;
    
    private Channel channel;

    public void start() throws CertificateException, SSLException, InterruptedException {

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler())
                    .childHandler(serverInitializer);

            channel = b.bind(config.getPort()).sync().channel();

            LOGGER.info("Open your web browser and navigate to {}://127.0.0.1:{}/",
                    (config.getSslEnabled() ? "https" : "http"), config.getPort());
            channel.closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}

package org.onetimelink.net;

import com.google.inject.Inject;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import java.util.Collections;
import org.onetimelink.entity.Page;
import org.onetimelink.service.PageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OnetimeLinkFullHttpHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    private final static Logger LOGGER = LoggerFactory.getLogger(OnetimeLinkFullHttpHandler.class);

    private FullHttpRequest request;

    @Inject
    private PageService pageService;

    public OnetimeLinkFullHttpHandler() {
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {
        request = msg;
        Page page;
        try {
            LOGGER.info("Request {} {}", request.method(), request.uri());
            LOGGER.debug(request.toString());

            if (request.method().equals(HttpMethod.GET)) {
                page = pageService.getPage(request.uri());
            } else {
                if (request.method().equals(HttpMethod.POST)) {
                    byte[] content = new byte[request.content().readableBytes()];
                    request.content().readBytes(content);
                    page = pageService.savePage(false, request.uri(), content, Collections.emptyMap());
                } else {
                    page = pageService.get(HttpResponseStatus.NOT_IMPLEMENTED);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Request processing error", ex);
            page = pageService.get(HttpResponseStatus.INTERNAL_SERVER_ERROR);
        }
        writeResponse(ctx, page);
    }

    private void writeResponse(ChannelHandlerContext ctx, Page page) {
        boolean keepAlive = HttpUtil.isKeepAlive(request);
        FullHttpResponse response = new DefaultFullHttpResponse(
                HTTP_1_1, HttpResponseStatus.valueOf(page.getResponseCode()),
                Unpooled.copiedBuffer(page.getContent()));
        page.getHeaders().entrySet().forEach((entry) -> {
            response.headers().set(entry.getKey(), entry.getValue());
        });
        if (keepAlive) {
            response.headers().setInt(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
            response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
        }
        ctx.write(response);
        if (!keepAlive) {
            ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
        }
    }

}

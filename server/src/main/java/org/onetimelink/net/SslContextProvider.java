package org.onetimelink.net;

import com.google.inject.Inject;
import com.google.inject.Provider;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslProvider;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import java.security.cert.CertificateException;
import javax.net.ssl.SSLException;
import org.onetimelink.config.ApplicationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SslContextProvider implements Provider<SslContext> {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(SslContextProvider.class);
    
    @Inject
    private ApplicationConfig config;
    
    @Override
    public SslContext get() {
        SslContext sslCtx = null;
        try {
            if (config.getSslEnabled()) {
                SelfSignedCertificate ssc = new SelfSignedCertificate();
                sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey())
                        .sslProvider(SslProvider.JDK).build();
            }
        } catch (CertificateException | SSLException ex) {
            LOGGER.warn("Can not get SSL context.", ex);
        }
        return sslCtx;
    }
    
}

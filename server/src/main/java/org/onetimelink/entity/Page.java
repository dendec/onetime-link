package org.onetimelink.entity;

import io.netty.handler.codec.http.HttpResponseStatus;
import java.util.HashMap;
import java.util.Map;

public class Page {

    private String path;
    private Boolean isPermanent;
    private Integer responseCode = HttpResponseStatus.OK.code();
    private byte[] content;
    private final Map<String, String> headers = new HashMap();

    public Page() {
    }

    public Page(String path, byte[] content) {
        this(false, path, content);
    }

    public Page(boolean isPermanent, String path, byte[] content) {
        this.isPermanent = isPermanent;
        this.path = path;
        this.content = content;
    }

    public Boolean getIsPermanent() {
        return isPermanent;
    }

    public byte[] getContent() {
        return content;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public String getPath() {
        return path;
    }

    public void addHeader(String name, String value) {
        headers.put(name, value);
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setIsPermanent(Boolean isPermanent) {
        this.isPermanent = isPermanent;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "Page{" + "path=" + path + ", isPermanent=" + isPermanent + '}';
    }
}

package org.onetimelink.persistence;

import org.onetimelink.entity.Page;

public interface PagePersistence extends KeyValueStorage<String, Page> {

    default public void put(Page page) {
        put(page.getPath(), page);
    }
    
}

package org.onetimelink.persistence;

import com.google.inject.Inject;
import com.google.inject.Provider;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.onetimelink.config.ApplicationConfig;
import org.onetimelink.entity.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PagePersistenceProvider implements Provider<PagePersistence> {

    private final static Logger LOGGER = LoggerFactory.getLogger(PagePersistenceProvider.class);

    private final static String PAGES_LOCATION = "/META-INF/resources/pages/";
    private final static String WEBJARS_LOCATION = "/META-INF/resources/webjars/";

    @Inject
    private ApplicationConfig applicationConfig;

    public PagePersistenceProvider() {
    }

    @Override
    public PagePersistence get() {
        PagePersistence result = new PagePersistenceMem();
        getPages(PAGES_LOCATION, Boolean.TRUE).stream().forEach(result::put);
        getPages(WEBJARS_LOCATION, Boolean.TRUE).stream().forEach(result::put);
        applicationConfig.getPageAliases().forEach((pageAlias) -> {
            LOGGER.info("Add page alias from {} to {}", pageAlias.getTarget(), pageAlias.getPath());
            Page page = result.get(pageAlias.getTarget());
            if (page != null) {
                result.put(pageAlias.getPath(), page);
            } else {
                LOGGER.error("Can not add alias. Page {} not exists.", pageAlias.getTarget());
            }

        });
        return result;
    }

    private List<Page> getPages(String location, Boolean isInternalResource) {
        try {
            if (isInternalResource) {
                URL url = getClass().getResource(location);
                if (url != null) {
                    URI uri = url.toURI();
                    try {
                        FileSystems.getFileSystem(uri);
                    } catch (FileSystemNotFoundException ex) {
                        FileSystems.newFileSystem(uri, Collections.EMPTY_MAP);
                    }
                    return scanPages(Paths.get(uri));
                } else {
                    LOGGER.error("Invalid internal resource location {}", location);
                }
            } else {
                File file = new File(location);
                if (file.exists() && file.isDirectory()) {
                    return scanPages(file.toPath());
                } else {
                    LOGGER.error("Invalid path to folder with pages {}", location);
                }
            }
        } catch (URISyntaxException | IOException | RuntimeException ex) {
            LOGGER.error("can not read content from {}", location, ex);
        }
        return Collections.EMPTY_LIST;
    }

    private List<Page> scanPages(Path pagesPath) throws IOException {
        LOGGER.info("Scan pages in {}", pagesPath);
        List<Path> pagePathes = Files.find(pagesPath,
                Integer.MAX_VALUE, (path, attr) -> !attr.isDirectory())
                .collect(Collectors.toList());
        List<Page> result = new ArrayList<>();
        for (Path pagePath : pagePathes) {
            Page page = new Page();
            String contentType = Files.probeContentType(pagePath);
            if (contentType != null) {
                page.addHeader(HttpHeaderNames.CONTENT_TYPE.toString(), contentType);
            }
            page.setContent(Files.readAllBytes(pagePath));
            page.setIsPermanent(true);
            page.setResponseCode(HttpResponseStatus.OK.code());
            page.setPath(pagePath.toString().replaceFirst(pagesPath.toString().replaceAll("/$", ""), ""));
            result.add(page);
        }
        return result;
    }
}

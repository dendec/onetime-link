package org.onetimelink.persistence;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.onetimelink.entity.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PagePersistenceMem implements PagePersistence {

    private final static Logger LOGGER = LoggerFactory.getLogger(PagePersistenceMem.class);

    private final Map<String, Page> pages = new ConcurrentHashMap<>();

    @Override
    public void put(String path, Page page) {
        LOGGER.info("{} added to {}", page, path);
        pages.put(path, page);
    }

    @Override
    public Page get(String path) {
        Page page = pages.get(path);
        LOGGER.info("{} returned", page);
        return page;
    }

    @Override
    public Page remove(String path) {
        Page page = pages.remove(path);
        LOGGER.info("{} deleted", page);
        return page;
    }

    @Override
    public Boolean has(String key) {
        return pages.containsKey(key);
    }
}

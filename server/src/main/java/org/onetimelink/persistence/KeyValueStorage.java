package org.onetimelink.persistence;

public interface KeyValueStorage<K, V> {

    public void put(K key, V value);

    public V get(K key);
    
    public Boolean has(K key);
    
    public V remove(K key);
}

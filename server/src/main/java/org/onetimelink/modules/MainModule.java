package org.onetimelink.modules;

import org.onetimelink.persistence.PagePersistenceProvider;
import com.google.inject.AbstractModule;
import org.onetimelink.config.ApplicationConfig;
import org.onetimelink.config.ApplicationConfigProvider;
import org.onetimelink.persistence.PagePersistence;
import org.onetimelink.service.PageService;

public class MainModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new NetModule());
        bind(ApplicationConfig.class).toProvider(ApplicationConfigProvider.class).asEagerSingleton();
        bind(PagePersistence.class).toProvider(PagePersistenceProvider.class).asEagerSingleton();
        bind(PageService.class).asEagerSingleton();
    }

}

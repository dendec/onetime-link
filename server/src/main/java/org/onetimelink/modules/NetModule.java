package org.onetimelink.modules;

import com.google.inject.AbstractModule;
import io.netty.handler.ssl.SslContext;
import org.onetimelink.net.OneTimeLinkServer;
import org.onetimelink.net.OnetimeLinkServerInitializer;
import org.onetimelink.net.SslContextProvider;

public class NetModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(OneTimeLinkServer.class).asEagerSingleton();
        bind(OnetimeLinkServerInitializer.class).asEagerSingleton();
        bind(SslContext.class).toProvider(SslContextProvider.class).asEagerSingleton();
    }
}

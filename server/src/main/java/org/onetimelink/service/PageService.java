package org.onetimelink.service;

import com.google.inject.Inject;
import io.netty.handler.codec.http.HttpResponseStatus;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.io.FilenameUtils;
import org.onetimelink.entity.Page;
import org.onetimelink.persistence.PagePersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageService {

    private final static Logger LOGGER = LoggerFactory.getLogger(PageService.class);

    private final static String ERROR_PAGES_LOCATION = "/META-INF/resources/error-pages/";

    @Inject
    private PagePersistence pagePersistence;

    private Map<HttpResponseStatus, Page> errorPages = new HashMap<>();

    public PageService() {
        URL url = getClass().getResource(ERROR_PAGES_LOCATION);
        if (url != null) {
            try {
                Path basePath = Paths.get(url.toURI());
                List<Path> errorPaths = Files.list(basePath).collect(Collectors.toList());
                for (Path errorPath : errorPaths) {
                    Page page = new Page();
                    Integer code = Integer.parseInt(FilenameUtils.getBaseName(errorPath.toString()));
                    page.setContent(Files.readAllBytes(errorPath));
                    page.setResponseCode(code);
                    errorPages.put(HttpResponseStatus.valueOf(code), page);
                }
            } catch (URISyntaxException | IOException ex) {
                LOGGER.error("can not read content of error pages", ex);
            }
        } else {
            LOGGER.error("can not find error pages at {}", ERROR_PAGES_LOCATION);
        }
    }

    public Page getPage(String path) {
        LOGGER.info("attempt to get {}", path);
        Page page = pagePersistence.get(path);
        if (page == null) {
            return get(HttpResponseStatus.NOT_FOUND);
        }
        if (!page.getIsPermanent()) {
            pagePersistence.remove(path);
        }
        return page;
    }

    public Page get(HttpResponseStatus status) {
        Page result = errorPages.get(status);
        if (result == null) {
            result = new Page();
            result.setContent(new byte[0]);
            result.setResponseCode(status.code());
            errorPages.put(status, result);
        }
        return result;
    }

    public Page savePage(boolean isPermanent, String path, byte[] content, Map<String, String> headers) {
        if (pagePersistence.has(path)) {
            return get(HttpResponseStatus.FORBIDDEN);
        } else {
            Page page = new Page(isPermanent, path, content);
            page.getHeaders().putAll(headers);
            pagePersistence.put(page);
            return get(HttpResponseStatus.CREATED);
        }
    }

    public Page savePage(boolean isPermanent, String path, InputStream content, Map<String, String> headers) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}

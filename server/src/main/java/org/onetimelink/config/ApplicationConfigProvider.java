package org.onetimelink.config;

import com.google.inject.Provider;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import java.util.ArrayList;

public class ApplicationConfigProvider implements Provider<ApplicationConfig> {

    private final Config config = ConfigFactory.load();

    @Override
    public ApplicationConfig get() {
        ApplicationConfig result = new ApplicationConfig();
        if (config.hasPath("port")) {
            result.setPort(config.getInt("port"));
        }
        if (config.hasPath("ssl-port")) {
            result.setSslPort(config.getInt("ssl-port"));
        }
        if (config.hasPath("ssl-enable")) {
            result.setSslEnabled(config.getBoolean("ssl-enable"));
        }
        result.setPageAliases(new ArrayList<>());
        config.getObjectList("aliases").forEach((object) -> {
            PageAliasConfig aliasConfig = new PageAliasConfig();
            aliasConfig.setPath(
                    object.get("path").unwrapped().toString());
            aliasConfig.setTarget(
                    object.get("target").unwrapped().toString());
            result.getPageAliases().add(aliasConfig);
        });
        return result;
    }

}

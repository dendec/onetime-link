package org.onetimelink.config;

public class PageAliasConfig {

    private String path;
    private String target;
    public PageAliasConfig() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "PageAliasConfig{" + "path=" + path + ", target=" + target + '}';
    }

}

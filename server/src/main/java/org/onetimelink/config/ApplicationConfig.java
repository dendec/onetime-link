package org.onetimelink.config;

import java.util.List;

public class ApplicationConfig {

    private List<PageAliasConfig> pageAliases;
    private Integer port;
    private Integer sslPort;
    private Boolean sslEnabled;

    public ApplicationConfig() {
    }

    public List<PageAliasConfig> getPageAliases() {
        return pageAliases;
    }

    public void setPageAliases(List<PageAliasConfig> pageAliases) {
        this.pageAliases = pageAliases;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getSslPort() {
        return sslPort;
    }

    public void setSslPort(Integer sslPort) {
        this.sslPort = sslPort;
    }

    public Boolean getSslEnabled() {
        return sslEnabled;
    }

    public void setSslEnabled(Boolean sslEnabled) {
        this.sslEnabled = sslEnabled;
    }

    @Override
    public String toString() {
        return "ApplicationConfig{" + "pageAliases=" + pageAliases + ", port=" + port + ", sslPort=" + sslPort + ", sslEnabled=" + sslEnabled + '}';
    }

}

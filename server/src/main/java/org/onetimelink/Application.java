package org.onetimelink;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.onetimelink.modules.MainModule;
import org.onetimelink.net.OneTimeLinkServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

    public static void main(String[] args) throws Exception {
        Injector injector = Guice.createInjector(new MainModule());
        injector.getInstance(OneTimeLinkServer.class).start();
    }

}

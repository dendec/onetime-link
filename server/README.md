# OnetimeLink file server
OnetimeLink is an an Open Source lightweight file sharing service based on netty network application framework.

## TODO
* ~~Add DI mechonism (Google Guice?)~~
* ~~Add configuration parsing (typesafe confing?)~~
* Add various persistence implementations(FS, S3, DB, etc)
* ~~Add predefined static web pages~~
* Add unit tests
* Add user interface
* Add streming uploading
* Add headers parsing
* Add content encryption
* Create deployment procedure(install script, deb/rpm package, etc)